﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using Sorting.Utils;

namespace Sorting.Benchmark
{
    [SimpleJob(RunStrategy.ColdStart, targetCount: 10)]
    [MinColumn, MaxColumn, MeanColumn, MedianColumn]
    public class SortingBenchmark
    {
        private const int N = 10000;
        private readonly int[] _data;

        private static readonly ISortingStrategy<int> InsertionSortingStrategy = new InsertionSortingStrategy<int>();
        private static readonly ISortingStrategy<int> MergeSortingStrategy = new MergeSortingStrategy<int>();
        private static readonly ISortingStrategy<int> QuickSortingStrategy = new QuickSortingStrategy<int>();

        public SortingBenchmark()
        {
            var random = new Random();
            _data = new int[N];
            for (var i = 0; i < N; i++)
            {
                _data[i] = random.Next(0, 10000);
            }
        }

        [Benchmark]
        public void Insertion() => InsertionSortingStrategy.Sort(_data.CloneArray());

        [Benchmark]
        public void Merge() => MergeSortingStrategy.Sort(_data.CloneArray());

        [Benchmark]
        public void Quick() => QuickSortingStrategy.Sort(_data.CloneArray());
    }
}