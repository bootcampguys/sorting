﻿using System;
using BenchmarkDotNet.Running;

namespace Sorting.Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<SortingBenchmark>();

            Console.ReadLine();
        }
    }
}
