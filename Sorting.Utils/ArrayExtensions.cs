﻿namespace Sorting.Utils
{
    public static class ArrayExtensions
    {
        public static T[] CloneArray<T>(this T[] original)
        {
            var clone = new T[original.Length];
            original.CopyTo(clone, 0);

            return clone;
        }
    }
}