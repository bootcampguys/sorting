# Task
To implement following sorting algorithms:
* Insertion sort
* Merge sort
* Quick sort (preferrable partition method: Hoare's partitioning)

# Benchmark
[BenchmarkDotNet](https://github.com/dotnet/BenchmarkDotNet) is used to benchmark sorting algorithms on different data sets and explore execution time and different cases.
In order to run it you need to set Sorting.Benchmark project as startup, pick Release configuration for build and run it.


You can change amount of data to check computational complexity of algorithms and compare them. Also you can prepare already sorted data or sorted by descending to check performance degradation and try to fix it if it's taking a place.

# Design
All sorting strategies are [generic classes](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/generic-classes) and can be used for any types if [IComparer<T>](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.icomparer-1?view=netframework-4.7.2) is provided. If comparer is not provided default one will be used instead. All validation checks are performed in base class and it's a proper place to put all common code for all strategies like `Swap` method. Also strategies can be composed together if you want to build ultimate one.

All strategies are supposed to be `allocation free` so they sort in-place and do not produce new arrays. Keep it in mind and it's the main reason of cloning arrays in tests and benchmarks.

# Literature
All information about `Insertion sort`, `Quick sort`, `Computational complexity`, `Big-O notation` can be found in `Introduction to Algorithms. CLRS` book