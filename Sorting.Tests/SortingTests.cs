﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Sorting.Utils;
using Xunit;

namespace Sorting.Tests
{
    public class SortingTests
    {
        [Theory, MemberData(nameof(GetSortData))]
        public void Sort_Test(ISortingStrategy<int> strategy, int[] data)
        {
            // Arrange
            var originalData = data.CloneArray();

            // Act
            strategy.Sort(data);

            // Assert
            Array.Sort(originalData);
            data.Should().BeEquivalentTo(originalData, opt => opt.WithStrictOrdering());
        }

        public static IEnumerable<object[]> GetSortData()
        {
            var strategies = new ISortingStrategy<int>[]
            {
                new InsertionSortingStrategy<int>(),
                //new MergeSortingStrategy<int>(),
                //new QuickSortingStrategy<int>()
            };

            var dataSets = new[]
            {
                new[] { 5, 6, 4, 5, -10, 3, -4, 3, -7, 2 },
                new[] { -4, 8, 7, 7, 7, -5, -6, 7, 9, -10 },
                new[] { 8, 4, 2, -4, -6, -8, -7, -7, -7, -3 },
                new[] { 7, 0, 0, 6, 9, -10, 1, -5, -10, 6 },
                new[] { -9, -5, -4, 3, -4, 2, 5, -7, 3, 3 }
            };

            // Cartesian product (strategies x dataSets)
            return dataSets.SelectMany(_ => strategies, (set, strategy) => new object[] { strategy, set.CloneArray() });
        }
    }
}