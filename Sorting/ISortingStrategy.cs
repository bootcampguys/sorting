﻿using System.Collections.Generic;

namespace Sorting
{
    public interface ISortingStrategy<T>
    {
        void Sort(T[] source, IComparer<T> comparer = null);

        void Sort(T[] source, int start, int end, IComparer<T> comparer = null);
    }
}