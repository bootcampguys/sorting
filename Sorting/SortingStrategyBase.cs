﻿using System;
using System.Collections.Generic;

namespace Sorting
{
    public abstract class SortingStrategyBase<T> : ISortingStrategy<T>
    {
        public void Sort(T[] source, IComparer<T> comparer = null)
        {
            Sort(source, 0, source.Length - 1, comparer);
        }

        public void Sort(T[] source, int start, int end, IComparer<T> comparer = null)
        {
            if (start < 0 || start > end) throw new ArgumentException(nameof(start));
            if (end >= source.Length)     throw new ArgumentException(nameof(end));

            comparer = comparer ?? Comparer<T>.Default;

            SortImpl(source, start, end, comparer);
        }

        protected abstract void SortImpl(T[] source, int start, int end, IComparer<T> comparer);
    }
}